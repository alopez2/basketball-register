var express = require('express');
var router = express.Router();
const fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://localhost:27017', function(err, client) {
  console.log("Connected successfully to server: Users");
 
  db = client.db('players');
});


router.get('/', (req, res) => {
	console.log('hit');
	var month = new Date().getMonth();
	res.setHeader('Content-Type', 'application/json');
	getAllPlayersForMonth(month, res);
});

getAllPlayersForMonth = (month, res) => {
	var query = {
		'_id': month
	};
	var players = db.collection('players').find(query).toArray((err, result) => {
		if (err) console.log(err);
		console.log(result);
		if (result[0]) {
			res.send(JSON.stringify(result[0].days));
		} else {
			res.send(JSON.stringify({data: 'empty'}));
		}
	});
}

module.exports = router;

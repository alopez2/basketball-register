var express = require('express');
var router = express.Router();
const fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var db;

MongoClient.connect('mongodb://localhost:27017', function(err, client) {
  console.log("Connected successfully to server: Register");
 
  db = client.db('players');
});

/* post sign up. */
router.post('/', (req, res, next) => {
	var body = req.body;
	
	//person - date obj
	var name = body.name;
	var email = body.email;
	var day = body.id;
	var month = new Date().getMonth();


	var query = {"_id": month};
	db.collection('players').find(query).toArray((err, result) => {
		if (err) console.log(err);
		if (result.length > 0) {
			checkDayForMonth(name, email, day, month);
		} else {
			addPlayer(name, email, day, month);
		}
		//res.redirect('/');
	});
});

//DONE
checkDayForMonth = (name, email, day, month) => {
	var query = {"_id": month, 'days.day': day};
	db.collection('players').find(query).toArray((err, result) => {
		if (err) console.log(err);
		if (result.length > 0) {
			var data = {_id: month, 'days.day': day};
			pushPlayer(data, name, email, day, month);
		} else {
			addNewPlayertoDate(name, email, day, month);
		}
	});
}

addNewPlayertoDate = (name, email, day, month) => {
	var query = {"_id": month};
	var data = {
		day: day,
		players: [ 
			{
				name: name,
				email: email
			}
		]
	};
	db.collection('players').update(query, {$push: {'days': data}});
}

addPlayer = (name, email, day, month) => {
	console.log('in else');
		var data = {
			_id: month,
			days: [
				{
					day: day,
					players: [ 
						{
							name: name,
							email: email
						}
					]
				}
			]
		};

		db.collection('players').save(data, (err, result) => {
			if (err) return console.log(err);
			console.log('saved');
		});
}

pushPlayer = (query, name, email, day, month) => {
	db.collection('players').update(
		query, 
		{$push: {'days.$.players': {name: name, email: email}}});
}

module.exports = router;

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      register: true,
      players: []
    };

    this.getAllPlayersForMonth();
  }

  registerUser = (id) => {
    this.setState({
      register: true
    });
    var name = document.getElementById('personName').value;
    var email = document.getElementById('personEmail').value;
    var data = {
      name: name,
      email: email,
      id: id.replace("day_", "")
    };
    console.log(id);
    fetch('/register', {
      body: JSON.stringify(data),
      cache: 'no-cache',
      credentials: 'include',
      headers: {
        'Accept-Language': 'en-US',
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST'
    })
    .then(response => response.json())
    .then(res => console.log(res));

    alert(`Registered: ${document.getElementById('personName').value}.`);

    document.getElementById('signupModal').style.display = "none";

  }

  openModal = (e, editId) => {
    console.log(e);
    var registerModal = document.getElementById('signupModal');
    var id = typeof editId === 'undefined' ? e.target.id : editId;
    var day = typeof editId === 'undefined' ? id.replace("day_", "") : editId;
    registerModal.style.display = "block";

    var title = document.getElementById('registerP');
    if (this.state.register) {
      title.innerHTML = `Register for ${months[new Date().getMonth()]} ${day}.`;
      document.getElementById('registerForm').onsubmit = this.registerUser.bind(this, id);
    } else {
      title.innerHTML = `Players for ${months[new Date().getMonth()]} ${day}.`;
    }
    document.getElementById('closeButton').onclick = () => {
      this.setState({
        register: true
      });
      registerModal.style.display = "none";
    }
  }

  openPlayersModal = (e) => {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

    var dayOfMonth = e.target.id.replace('num-players-', '');
    var players = [];
    console.log(dayOfMonth);
    console.log(this.state);

    this.state.users.forEach((day) => {
      if (day.day === dayOfMonth) {
        players = day.players;
      }
    });

    this.setState({
      register: false,
      players: players
    });
    
    this.openModal(e, dayOfMonth);
  }

  getAllPlayersForMonth = () => {
    fetch('/users', {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Network response was not ok.');
    }).then((res) => {
      if (res.data === 'empty') {
        this.renderCalendar();
        return;
      }
      this.setState({
        users: res
      });
      this.renderCalendar();
      this.countPlayersPerDay(this.state.users);
    });
  }

  countPlayersPerDay = (days) => {
    console.log(days);
    days.forEach((day) => {
      var date = document.getElementById(`day_${day.day}`)
      var numPlayers = document.createElement('span');
      numPlayers.className = 'num-players';
      numPlayers.id = `num-players-${day.day}`;
      numPlayers.innerHTML = `${day.players.length} players`;
      numPlayers.onclick = this.openPlayersModal;

      date.appendChild(numPlayers);
    });
  }

  renderCalendar = () => {
    var monthDays = [31,28,31,30,31,30,31,31,30,31,30,31]
    var parent = document.getElementById("calendar");
    var monthStart = new Date(new Date().getFullYear(), new Date().getMonth(), '01').getDay();
    var monthEnd = monthDays[new Date().getMonth()];
    console.log(monthStart);
    
    var monthDay = 0;
    var incDate = false;
    for (var i = 1; i <= 35; i++) {
      if (incDate) {
        monthDay++;
      }
      var dayOfWeek = new Date(new Date().getFullYear(), new Date().getMonth(), monthDay).getDay();
      if (dayOfWeek === 0 || dayOfWeek === 6) {
        continue;
      }
      var child = document.createElement('div');
      
      child.className = 'day-of-week';
      
      if (i >= monthStart && monthDay <= monthEnd) {
        if (!incDate) {
          monthDay++;
        }
        incDate = true;
        child.id = `day_${monthDay}`;
        child.className += ' real-day';
        var day = document.createElement('p');
        day.id = monthDay;
        day.innerHTML = monthDay;
        child.appendChild(day);
        child.onclick = this.openModal;
      }
      parent.appendChild(child);
    }
  }

  render() {
    var d = new Date();
    return (
      <div className="App">
        <h1>{months[d.getMonth()]}</h1>
        <div id="calendar-header">
          <div className="dayOfWeekHeader">
            <p>Monday</p>
          </div>
          <div className="dayOfWeekHeader">
            <p>Tuesday</p>
          </div>
          <div className="dayOfWeekHeader">
            <p>Wednesday</p>
          </div>
          <div className="dayOfWeekHeader">
            <p>Thursday</p>
          </div>
          <div className="dayOfWeekHeader">
            <p>Friday</p>
          </div>
        </div>
        <div id="calendar">
        </div>
        <div id="signupModal" className="modal">
          <div className="modal-content">
            <span className="close" id="closeButton">&times;</span>
            <div className="modal-title">
              <p id="registerP"></p>
            </div>
            <div className="modal-body">
              {this.state.register ?
                <form id="registerForm">
                  <input className="effect-3" type="text"id="personName" placeholder="Name" required="required" />
                  <br/>
                  <input className="effect-3" type="email" id="personEmail" placeholder="Email" required="required" />
                  <br/>
                  <input id="sign-up" type="submit" value="sign up" />
                </form>
              :
                <div className="listOfPlayers">
                  {this.state.players.map((listValue) => {
                    return <li>{listValue.name}</li>;
                  })}
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
